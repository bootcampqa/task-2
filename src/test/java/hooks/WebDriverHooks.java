package hooks;

import android_driver.AndroidDriverInstance;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeAll;


public class WebDriverHooks {

    @Before
    public void initializeAndroidDriver() {
        AndroidDriverInstance.initialize();
    }

    @After
    public void quitAndroidDriver() {
        AndroidDriverInstance.quit();
    }
}