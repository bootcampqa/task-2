Feature: Login Feature
  Scenario: Success Login
    Given User click entry point login
    When User input username "lordlord"
    When User input password "stockbit"
    And User click login page
    Then Header will be appear
  Scenario: Login with invalid username
    Given User click entry point login
    When User input username "smkvsdjsdf"
    When User input password "stockbit"
    And User click login page
    Then Message error "Username atau password salah. Mohon coba lagi." will be appear
  Scenario: Login with invalid password
    Given User click entry point login
    When User input username "lordlord"
    When User input password "sdfsdfsdf"
    And User click login page
    Then Message error "Username atau password salah. Mohon coba lagi." will be appear
  Scenario: Login with blank username and password
    Given User click entry point login
    When User input username ""
    When User input password ""
    And User click login page
    Then Message error "Mohon masukkan username/email dan password kamu." will be appear