package base;

import android_driver.AndroidDriverInstance;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;


public class BasePageObject {

    public AndroidDriver driver() {
        return AndroidDriverInstance.androidDriver;
    }

    public void inputText(By element, String text) {
        driver().findElement(element).sendKeys(text);
    }

    public void tap(By element) {
        driver().findElement(element).click();
    }

    public void isDisplayed(By element) {
        driver().findElement(element).isDisplayed();
    }

    public void printError(String text) {
        throw new AssertionError(text);
    }

    public void isTextSame(By element, String text) {
        String getText = getText(element);
        if (!getText.equals(text)) {
            printError(String.format("Expected string [%s] is not match with actual string [%s]", text, getText));
        }
    }

    public String getText(By element) {
        return driver().findElement(element).getText();
    }
}
