package page_object;

import base.BasePageObject;
import org.openqa.selenium.By;

public class LoginPage extends BasePageObject {

    public void clickLoginEntryPoint() {
       //isDisplayed(By.id("com.stockbit.android:id/ivFragmentStreamCompanyLogo"));
        tap(By.id("com.stockbitdev.android:id/btnWellcomeLogIn"));
    }

    public void inputUsername(String username) {
        inputText(By.xpath("(//android.widget.EditText[contains(@resource-id, 'tiet_text_field_input')])[1]"), username);
    }

    public void inputPassword(String password) {
        inputText(By.xpath("(//android.widget.EditText[contains(@resource-id, 'tiet_text_field_input')])[2]"), password);
    }

    public void clickButtonLogin() {
        tap(By.id("com.stockbitdev.android:id/tv_login_submit"));
    }

    public void headerDisplayed() {
        isDisplayed(By.id("com.stockbitdev.android:id/vg_header"));
    }
    public void checkAlert(String alert){
        isTextSame(By.id("com.stockbitdev.android:id/tv_text_field_helper"), alert);
    }

}